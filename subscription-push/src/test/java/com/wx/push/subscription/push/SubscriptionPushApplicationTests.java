package com.wx.push.subscription.push;

import com.wx.push.subscription.push.bo.JsonRootBean;
import com.wx.push.subscription.push.mybatisplus.service.IUserInfoService;
import com.wx.push.subscription.push.utils.BaiduWeatherUtil;
import com.wx.push.subscription.push.utils.ImportantTimeUtil;
import com.wx.push.subscription.push.utils.MsgApiUtil;
import com.wx.push.subscription.push.utils.PusherUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SubscriptionPushApplicationTests {

    @Autowired
    IUserInfoService userInfoService;

    @Test
    public void testGetBirthDay(){
        String hasTime = ImportantTimeUtil.hasTime("2017-11-25");
        System.out.println(hasTime);


//        String birthDay = ImportantTimeUtil.getBirthDay("1998-09-10");
    }


    @Test
    void testConfig(){
        PusherUtil pusherUtil = new PusherUtil();
        pusherUtil.getSubscriptionConfig();


    }

    @Test
    void testSQl(){
        userInfoService.pushMsgToEveryUser();
    }



    @Test
    void contextLoads() {
        String caihongpi = MsgApiUtil.getAMessage("http://api.tianapi.com/caihongpi/index", "3d6f98e93c56503d2e40d3ca8eb3972c","");
        String healthtip = MsgApiUtil.getAMessage("http://api.tianapi.com/healthtip/index", "3d6f98e93c56503d2e40d3ca8eb3972c","");
        String star = MsgApiUtil.getAMessage("http://api.tianapi.com/star/index", "3d6f98e93c56503d2e40d3ca8eb3972c","&astro=taurus");
        String lzmy = MsgApiUtil.getAMessage("http://api.tianapi.com/lzmy/index", "3d6f98e93c56503d2e40d3ca8eb3972c","");
        System.out.println(caihongpi);
        System.out.println(healthtip);
        System.out.println(star);
        System.out.println(lzmy);
    }

    @Test
    public void testWeather() {
        JsonRootBean weather = BaiduWeatherUtil.getWeather("110105");
        System.out.println(weather);
    }

}
