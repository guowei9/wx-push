package com.wx.push.subscription.push.mybatisplus.controller;


import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wx.push.subscription.push.mybatisplus.entity.SendMsg;
import com.wx.push.subscription.push.mybatisplus.entity.UserInfo;
import com.wx.push.subscription.push.mybatisplus.mapper.UserInfoMapper;
import com.wx.push.subscription.push.mybatisplus.service.ISendMsgService;
import com.wx.push.subscription.push.mybatisplus.service.IUserInfoService;
import com.wx.push.subscription.push.utils.PusherUtil;
import com.wx.push.subscription.push.vo.Result;
import com.wx.push.subscription.push.vo.SendMsgVo;
import com.wx.push.subscription.push.vo.UserInfoVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.time.LocalDateTime;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author phil
 * @since 2022-08-23
 */
@Slf4j
@RestController
@RequestMapping("/sendMsg")
public class SendMsgController {

    @Autowired
    ISendMsgService sendMsgService;
    @Autowired
    IUserInfoService userInfoService;
    @Autowired
    UserInfoMapper userInfoMapper;


    @GetMapping("/add")
    public Result addSendMsg(@RequestParam String sendName,String sendMsg){
        SendMsg sendMsgEntity = new SendMsg();
        sendMsgEntity.setSendName(sendName);
        sendMsgEntity.setSendMsg(sendMsg);
//        BeanUtils.copyProperties(sendMsgVo, sendMsg);
        UserInfo userInfo = userInfoService.getOne(new QueryWrapper<UserInfo>()
                .like("send_name", sendName).orderByDesc("update_time").last("limit 1"));;
        sendMsgEntity.setUserId(userInfo.getUserId());
        sendMsgEntity.setCreateTime(LocalDateTime.now());
        sendMsgEntity.setUpdateTime(LocalDateTime.now());
        boolean save = sendMsgService.saveOrUpdate(sendMsgEntity);
        if (save) {
            log.info("向：{}留言了一条:{}消息",sendMsgEntity.getSendName(),sendMsgEntity.getSendMsg());
            return new Result().ok();
        }else {
            return new Result().fail("发送失败");
        }
    }
    @GetMapping("/now")
    public Result sendMsgNow(@RequestParam String sendName,String sendMsg){
        SendMsg sendMsgEntity = new SendMsg();
        sendMsgEntity.setSendName(sendName);
        sendMsgEntity.setSendMsg(sendMsg);
        UserInfo userInfo = userInfoService.getOne(new QueryWrapper<UserInfo>()
                .like("send_name", sendName).orderByDesc("update_time").last("limit 1"));;
        sendMsgEntity.setUserId(userInfo.getUserId());
        sendMsgEntity.setCreateTime(LocalDateTime.now());
        sendMsgEntity.setUpdateTime(LocalDateTime.now());

        boolean save = sendMsgService.saveOrUpdate(sendMsgEntity);

        UserInfoVo userInfoVo = userInfoMapper.selectBySendName(sendName);
        PusherUtil.pushMessage(userInfoVo);
//        Result result = userInfoService.pushMsgToOneNow(userInfoVo);
        if (save) {
            log.info("向：{}发送了一条:{}消息",userInfoVo.getUserName(),userInfoVo.getSendMsgVo().getSendMsg());
            return new Result().ok();
        }else {
            return new Result().fail("发送失败");
        }
    }

}
