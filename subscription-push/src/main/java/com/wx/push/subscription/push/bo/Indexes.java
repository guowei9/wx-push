/**
  * Copyright 2022 json.cn 
  */
package com.wx.push.subscription.push.bo;

/**
 * Auto-generated: 2022-08-22 14:17:10
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
public class Indexes {

    private String name;
    private String brief;
    private String detail;
    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setBrief(String brief) {
         this.brief = brief;
     }
     public String getBrief() {
         return brief;
     }

    public void setDetail(String detail) {
         this.detail = detail;
     }
     public String getDetail() {
         return detail;
     }

}