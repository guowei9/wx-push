package com.wx.push.subscription.push.mybatisplus.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wx.push.subscription.push.mybatisplus.entity.UserInfo;
import com.wx.push.subscription.push.mybatisplus.mapper.UserInfoMapper;
import com.wx.push.subscription.push.mybatisplus.service.ISendMsgService;
import com.wx.push.subscription.push.mybatisplus.service.IUserInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.push.subscription.push.utils.PusherUtil;
import com.wx.push.subscription.push.vo.Result;
import com.wx.push.subscription.push.vo.UserInfoVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.PublicKey;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author phil
 * @since 2022-08-22
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements IUserInfoService {

    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private ISendMsgService sendMsgService;

    /**
     * 给库里所有用户发消息
     */
    @Override
    public void pushMsgToEveryUser() {
        List<UserInfoVo> userList = userInfoMapper.selectUserAndSendMsg();
        for (UserInfoVo userInfoVo : userList) {
            PusherUtil.pushMessage(userInfoVo);
        }
    }

    @Override
    public Result pushMsgToOneNow(UserInfoVo userInfoVo) {

        PusherUtil.pushMessage(userInfoVo);
        return Result.ok();
    }
}
