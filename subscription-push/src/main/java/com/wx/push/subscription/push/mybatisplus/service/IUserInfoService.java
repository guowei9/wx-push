package com.wx.push.subscription.push.mybatisplus.service;

import com.wx.push.subscription.push.mybatisplus.entity.UserInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wx.push.subscription.push.vo.Result;
import com.wx.push.subscription.push.vo.UserInfoVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author phil
 * @since 2022-08-22
 */

public interface IUserInfoService extends IService<UserInfo> {
    public abstract void pushMsgToEveryUser();
    Result pushMsgToOneNow(UserInfoVo userInfoVo);
}
