package com.wx.push.subscription.push.utils;

import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author 01424605
 * @create 2022-08-22 14:32
 */
@Slf4j
public class ImportantTimeUtil {

    /**
     * 已经过去date多少天
     * @param date
     * @return
     */
    public static String hasTime(String date) {
        SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Date preDate = null;
        try {
            preDate = myFormatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            log.info("转换纪念日时间失败:{}",date);
        }
        String dateDiff = getDateDiff(preDate, new Date());

        return dateDiff;
    }

    public static String getDateDiff(Date preDate, Date nowDate) {
        if(preDate == null)return "";
        long ny = 1000L * 365* 24 * 60 * 60;
        long nmon = 1000L * 30 * 24 * 60 * 60;
        long nd = 1000L * 24 * 60 * 60;
        long nh = 1000L * 60 * 60;
        long nm = 1000L * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = Math.abs(nowDate.getTime() - preDate.getTime());
        //计算多少年
        long year = diff / ny;
        //计算多少月
        long month = diff % ny / nmon;
        // 计算差多少天
        long day = diff % nmon / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;
        return year + "年" + month + "个月"+ day + "天" + hour + "小时" + min + "分钟";
    }


    public static String getBirthDay(String tarTime) {
        String[] split = tarTime.split("-");
        Date date = new Date();
        Calendar instance = Calendar.getInstance();
        int i = instance.get(Calendar.YEAR);
        String year = String.valueOf(i);
        String nowBirthDay = year + split[1] + split[2];
        String solarBirthDay = "";
        try {
            solarBirthDay = CalendarUtil.lunarToSolar(nowBirthDay, false);//阴历转阳历
        } catch (Exception e) {
            e.printStackTrace();
        }
        int days = 0;
        try {
            SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
            String clidate = solarBirthDay;
            Calendar cToday = Calendar.getInstance(); // 存今天
            Calendar cBirth = Calendar.getInstance(); // 存生日
            cBirth.setTime(myFormatter.parse(clidate)); // 设置生日
            cBirth.set(Calendar.YEAR, cToday.get(Calendar.YEAR)); // 修改为本年
            if (cBirth.get(Calendar.DAY_OF_YEAR) < cToday.get(Calendar.DAY_OF_YEAR)) {
                // 生日已经过了，要算明年的了
                days = cToday.getActualMaximum(Calendar.DAY_OF_YEAR) - cToday.get(Calendar.DAY_OF_YEAR);
                days += cBirth.get(Calendar.DAY_OF_YEAR);
            } else {
                // 生日还没过
                days = cBirth.get(Calendar.DAY_OF_YEAR) - cToday.get(Calendar.DAY_OF_YEAR);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return String.valueOf(days);
    }

    

}
