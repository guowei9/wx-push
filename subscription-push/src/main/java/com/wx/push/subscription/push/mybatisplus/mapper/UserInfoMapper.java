package com.wx.push.subscription.push.mybatisplus.mapper;

import com.wx.push.subscription.push.mybatisplus.entity.UserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.push.subscription.push.vo.UserInfoVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author phil
 * @since 2022-08-22
 */
@Repository
public interface UserInfoMapper extends BaseMapper<UserInfo> {

    List<UserInfoVo> selectUserAndSendMsg();

    UserInfoVo selectBySendName(@Param("sendName") String sendName);
}
