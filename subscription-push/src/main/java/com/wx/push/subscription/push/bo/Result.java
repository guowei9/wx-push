/**
  * Copyright 2022 json.cn 
  */
package com.wx.push.subscription.push.bo;
import java.util.List;

/**
 * Auto-generated: 2022-08-22 14:17:10
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
public class Result {

    private Location location;
    private Now now;
    private List<Indexes> indexes;
    private List<Alerts> alerts;
    private List<Forecasts> forecasts;
    private List<Forecast_hours> forecast_hours;
    public void setLocation(Location location) {
         this.location = location;
     }
     public Location getLocation() {
         return location;
     }

    public void setNow(Now now) {
         this.now = now;
     }
     public Now getNow() {
         return now;
     }

    public void setIndexes(List<Indexes> indexes) {
         this.indexes = indexes;
     }
     public List<Indexes> getIndexes() {
         return indexes;
     }

    public void setAlerts(List<Alerts> alerts) {
         this.alerts = alerts;
     }
     public List<Alerts> getAlerts() {
         return alerts;
     }

    public void setForecasts(List<Forecasts> forecasts) {
         this.forecasts = forecasts;
     }
     public List<Forecasts> getForecasts() {
         return forecasts;
     }

    public void setForecast_hours(List<Forecast_hours> forecast_hours) {
         this.forecast_hours = forecast_hours;
     }
     public List<Forecast_hours> getForecast_hours() {
         return forecast_hours;
     }

}