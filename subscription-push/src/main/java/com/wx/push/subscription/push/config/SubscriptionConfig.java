package com.wx.push.subscription.push.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @author 01424605
 * @create 2022-08-22 17:07
 */

//@ConfigurationProperties(prefix = "subscription")
@PropertySource("classpath:my.properties")
@Data
@Component
public class SubscriptionConfig {
    @Value("${subscription.appId}")
    public static final String appId ="kong";
    static String secret;
    static String morningTemplateId;
}


