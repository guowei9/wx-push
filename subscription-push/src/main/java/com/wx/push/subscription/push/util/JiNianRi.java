package com.wx.push.subscription.push.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @ClassName JiNianRi
 * @Description TODO
 * @Author 张辉
 * @Date 2022/8/2 17:32
 */
public class JiNianRi {
    /**
     * 恋爱
     */
    static String lianAi = "2017-11-25";
//    /**
//     * 领证
//     */
//    static String linZheng = "2022-03-19";
//    /**
//     * 结婚
//     */
//    static String jieHun = "2022-07-08";
    /**
     * 生日
     */
    static String shengRi = "1998-09-05";//阳历

    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 距离date还有多少天
     * @param date
     * @return
     */
    public static String  before(String date) {
        long day = 0;
        try {
            long nd = 1000 * 24 * 60 * 60;
            // 获得两个时间的毫秒时间差异
            long diff = simpleDateFormat.parse(date).getTime() -  System.currentTimeMillis();
            // 计算差多少天
            day = diff / nd;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String newDay = String.valueOf(day).replace("-","");
        return newDay;
    }


    /**
     * 已经过去date多少天
     * @param date
     * @return
     */
    public static String after(String date) {
        long day = 0;
        try {
            long nd = 1000 * 24 * 60 * 60;
            // 获得两个时间的毫秒时间差异
            long diff =  System.currentTimeMillis() - simpleDateFormat.parse(date).getTime();
            // 计算差多少天
            day = diff / nd;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String newDay = String.valueOf(day).replace("-","");
        return newDay;
    }

//    public static int getJieHun() {
//        return before(jieHun);
//    }
//
//    public static int getLinZhen() {
//        return before(linZheng);
//    }

    public static String getLianAi() {
//        return before(lianAi);
        return String.valueOf(getBirthDay(lianAi));
    }

    public static String getShengRi(){
        return String.valueOf(getBirthDay(shengRi));
//        return after(shengRi);
    }

//    public static void main(String[] args) {
//        System.out.println(getJieHun());
//    }
public static int getBirthDay(String addtime) {
    int days = 0;
    try {
        SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
        String clidate = addtime;
        Calendar cToday = Calendar.getInstance(); // 存今天
        Calendar cBirth = Calendar.getInstance(); // 存生日
        cBirth.setTime(myFormatter.parse(clidate)); // 设置生日
        cBirth.set(Calendar.YEAR, cToday.get(Calendar.YEAR)); // 修改为本年
        if (cBirth.get(Calendar.DAY_OF_YEAR) < cToday.get(Calendar.DAY_OF_YEAR)) {
            // 生日已经过了，要算明年的了
            days = cToday.getActualMaximum(Calendar.DAY_OF_YEAR) - cToday.get(Calendar.DAY_OF_YEAR);
            days += cBirth.get(Calendar.DAY_OF_YEAR);
        } else {
            // 生日还没过
            days = cBirth.get(Calendar.DAY_OF_YEAR) - cToday.get(Calendar.DAY_OF_YEAR);
        }
    } catch (ParseException e) {
        e.printStackTrace();
    }
    return days;
}


}
