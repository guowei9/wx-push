package com.wx.push.subscription.push.mybatisplus.mapper;

import com.wx.push.subscription.push.mybatisplus.entity.SendMsg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author phil
 * @since 2022-08-23
 */
public interface SendMsgMapper extends BaseMapper<SendMsg> {

}
