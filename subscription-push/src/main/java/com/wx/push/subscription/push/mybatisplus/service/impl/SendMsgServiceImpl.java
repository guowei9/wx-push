package com.wx.push.subscription.push.mybatisplus.service.impl;

import com.wx.push.subscription.push.mybatisplus.entity.SendMsg;
import com.wx.push.subscription.push.mybatisplus.mapper.SendMsgMapper;
import com.wx.push.subscription.push.mybatisplus.service.ISendMsgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author phil
 * @since 2022-08-23
 */
@Service
public class SendMsgServiceImpl extends ServiceImpl<SendMsgMapper, SendMsg> implements ISendMsgService {

}
