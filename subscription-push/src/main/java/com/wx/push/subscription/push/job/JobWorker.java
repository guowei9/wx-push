package com.wx.push.subscription.push.job;

import com.wx.push.subscription.push.mybatisplus.service.IUserInfoService;
import com.wx.push.subscription.push.util.Pusher;
import com.wx.push.subscription.push.utils.ImportantTimeUtil;
import com.wx.push.subscription.push.utils.MsgApiUtil;
import com.wx.push.subscription.push.utils.PusherUtil;
import com.wx.push.subscription.push.vo.UserInfoVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


/**
 *@ClassName JobWorker
 *@Description TODO
 *@Author 张辉
 *@Date 2022/8/2 16:00
 */
@Slf4j
@Component
public class JobWorker {


    @Autowired
    IUserInfoService userInfoService;

    @Scheduled(cron = "0 0 8 * * ?")//"0 0 8 * * ?"
    public void pusherAfternoonMsg2(){
        log.info("早上8点，每日一言");
        userInfoService.pushMsgToEveryUser();
    }

//    @Scheduled(cron = "0 0 14 * * ?")//"0 0 8 * * ?"
    public void pusherAfternoonMsg1(){
        log.info("下午2点，每日一言");
        userInfoService.pushMsgToEveryUser();
    }

//    @Scheduled(cron = "0 30 17 * * ?")//"0 0 8 * * ?"
    public void pusherAfternoonMsg3(){

        userInfoService.pushMsgToEveryUser();
    }

    //要推送的用户openid
    private static String openidMy = "ofbVB55jygAEF_7vR9BO0eARMFHc";
    private static String openidBaby = "ofbVB5_czGmCeonlSwSuTP-_LPy8";


    private static String memorialTime = "2017-11-25";
//    @Scheduled(cron = "0 0 8 * * ?")//"0 0 8 * * ?"
    public void pusherMsg(){

        UserInfoVo userGw = new UserInfoVo();
        userGw.setOpenId(openidMy);
        userGw.setUserName("郭威");
        userGw.setBirthdayTime("1998-03-19");
        userGw.setCityCode("110105");
        String memorialDay = ImportantTimeUtil.hasTime(memorialTime);
        String aMessage = MsgApiUtil.getAMessage("http://api.tianapi.com/saylove/index", "3d6f98e93c56503d2e40d3ca8eb3972c", "");
        userGw.setUniqueMsg("独有的情话❤️"+ "："+aMessage);
        userGw.setMemorialDayMsg("今天是我们恋爱的第"+memorialDay+"天");
        PusherUtil.pushMessage(userGw);

        UserInfoVo userSh = new UserInfoVo();
        userSh.setOpenId(openidBaby);
        userSh.setUserName("亲爱的慧慧小宝贝");
        userSh.setBirthdayTime("1998-09-05");
        userSh.setCityCode("130984");
        userSh.setMemorialDayMsg("今天是我们恋爱的第"+memorialDay+"天");
        userSh.setUniqueMsg("独有的情话：❤️"+aMessage);
        userSh.setMsg("今天依旧是超级爱你的一天喔~");
        PusherUtil.pushMessage(userSh);

        UserInfoVo userSj = new UserInfoVo();
        userSj.setOpenId("ofbVB5y7HqPh8BdvhV83adnMad9w");
        userSj.setUserName("石佳");
        userSj.setCityCode("130984");
        userSj.setMsg("石佳今天美美哒~");
        PusherUtil.pushMessage(userSj);

    }

//
//    @Scheduled(cron = "0 30 20 * * ?")//"0 0 8 * * ?"
//    public void pusherAfternoonMsg1(){
//        log.info("定时：{}",new Date().toString());
//
//        UserInfoVo userGw = new UserInfoVo();
//        userGw.setOpenId(openidMy);
//        userGw.setUserName("郭威");
//        userGw.setBirthdayTime("1998-03-19");
//        userGw.setCityCode("110105");
//        String aMessage = MsgApiUtil.getAMessage("http://api.tianapi.com/saylove/index", "3d6f98e93c56503d2e40d3ca8eb3972c", "");
//        userGw.setUniqueMsg("独有的情话❤️"+ "："+aMessage);
//        PusherUtil.pushMessage(userGw);
//    }

}
