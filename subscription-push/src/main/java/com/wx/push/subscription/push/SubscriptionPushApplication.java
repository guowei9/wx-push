package com.wx.push.subscription.push;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@MapperScan(value="com/wx/push/subscription/push/mybatisplus/mapper")
public class SubscriptionPushApplication {

    //要推送的用户openid
    private static String openidMy = "ofbVB55jygAEF_7vR9BO0eARMFHc";
    private static String openidBaby = "ofbVB5_czGmCeonlSwSuTP-_LPy8";

    private static String memorialTime = "2017-11-25";
    public static void main(String[] args) {
//        UserInfo userGw = new UserInfo();
//        userGw.setOpenId(openidMy);
//        userGw.setName("郭威");
//        userGw.setBirthdayTime("1998-03-19");
//        userGw.setCityCode("110105");
//        String aMessage = MsgApiUtil.getAMessage("http://api.tianapi.com/saylove/index", "3d6f98e93c56503d2e40d3ca8eb3972c", "");
//        userGw.setUniqueMsg("独有的情话❤️"+ "："+aMessage);
//        PusherUtil.pushMessage(userGw);
//
//        UserInfo userSh = new UserInfo();
//        userSh.setOpenId(openidBaby);
//        userSh.setName("亲爱的慧慧小宝贝");
//        userSh.setBirthdayTime("1998-09-05");
//        userSh.setCityCode("130984");
//        String memorialDay = ImportantTimeUtil.hasTime(memorialTime);
//        userSh.setLoveMsg("今天是我们恋爱的第"+memorialDay+"天");
//        userGw.setUnique("独有的情话：❤️"+aMessage);
//        userSh.setMsg("今天依旧是超级爱你的一天喔~");
//        pusherUtil.pushMessage(userSh);
//
//        UserInfo userSj = new UserInfo();
//        userSj.setOpenId("ofbVB5y7HqPh8BdvhV83adnMad9w");
//        userSj.setName("石佳");
//        userSj.setCityCode("130984");
//        userSj.setMsg("石佳今天美美哒~");
//        pusherUtil.pushMessage(userSj);

        SpringApplication.run(SubscriptionPushApplication.class, args);
    }

}
