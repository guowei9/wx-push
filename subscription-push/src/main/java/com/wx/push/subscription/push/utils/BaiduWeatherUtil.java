package com.wx.push.subscription.push.utils;

import com.alibaba.fastjson.JSONObject;
import com.wx.push.subscription.push.bo.JsonRootBean;
import com.wx.push.subscription.push.util.MyHttpClientUtils;

/**
 * @author 01424605
 * @create 2022-08-22 14:19
 */
public class BaiduWeatherUtil {
//    todo 配置类
    private static final String ak = "lOqgoL2z7g6TkU9HIErESlVttvdzQpSw";
    public static JsonRootBean getWeather(String district_id) {
        String str = MyHttpClientUtils.sendGet("https://api.map.baidu.com/weather/v1/?district_id=" + district_id + "&data_type=all&ak=" + ak);
        JsonRootBean parse = JSONObject.parseObject(str, JsonRootBean.class);
        if(parse.getStatus() == 0 || parse.getMessage().equals("success")) return parse;
        else return null;
    }
}
