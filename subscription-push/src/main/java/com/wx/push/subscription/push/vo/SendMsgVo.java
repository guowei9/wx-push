package com.wx.push.subscription.push.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author phil
 * @since 2022-08-23
 */
@Data
public class SendMsgVo{

    private Integer id;
    /**
     * user_id
     */
    private Integer userId;
    private String sendName;

    private String sendMsg;

    /**
     * 是否删除，0 未删除，1 已删除
     */
    private Integer isDelete;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


    @Override
    public String toString() {
        return "SendMsg{" +
            ", userId=" + userId +
            ", sendMsg=" + sendMsg +
            ", isDelete=" + isDelete +
            ", createTime=" + createTime +
            ", updateTime=" + updateTime +
        "}";
    }
}
