/**
  * Copyright 2022 json.cn 
  */
package com.wx.push.subscription.push.bo;

/**
 * Auto-generated: 2022-08-22 14:17:10
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
public class Alerts {

    private String type;
    private String level;
    private String title;
    private String desc;
    public void setType(String type) {
         this.type = type;
     }
     public String getType() {
         return type;
     }

    public void setLevel(String level) {
         this.level = level;
     }
     public String getLevel() {
         return level;
     }

    public void setTitle(String title) {
         this.title = title;
     }
     public String getTitle() {
         return title;
     }

    public void setDesc(String desc) {
         this.desc = desc;
     }
     public String getDesc() {
         return desc;
     }

}