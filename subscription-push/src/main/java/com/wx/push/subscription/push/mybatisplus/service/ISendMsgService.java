package com.wx.push.subscription.push.mybatisplus.service;

import com.wx.push.subscription.push.mybatisplus.entity.SendMsg;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wx.push.subscription.push.vo.UserInfoVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author phil
 * @since 2022-08-23
 */
public interface ISendMsgService extends IService<SendMsg> {

}
