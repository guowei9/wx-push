package com.wx.push.subscription.push.mybatisplus.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author phil
 * @since 2022-08-22
 */
@Data
@TableName("t_user_info")
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "user_id", type = IdType.AUTO)
    private Integer userId;

    private String userName;
    private String sendName;

    private String openId;

    /**
     * 默认北京朝阳
     */
    private String cityCode;

    private String birthdayTime;

    private String msg;

    private String memorialDayMsg;

    private String uniqueMsg;

    /**
     * 是否删除，0 未删除，1 已删除
     */
    private Integer isDelete;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

}
