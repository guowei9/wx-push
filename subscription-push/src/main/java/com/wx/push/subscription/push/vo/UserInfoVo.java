package com.wx.push.subscription.push.vo;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author 01424605
 * @create 2022-08-22 14:43
 */
@Data
public class UserInfoVo {
    private Integer userId;
    private String userName;
    private String sendName;
    private String openId;
    /**
     * 默认北京朝阳
     */
    private String cityCode;
    private String birthdayTime;
    private String msg;
    private String memorialDayMsg;
    private String uniqueMsg;

    /**
     * 是否删除，0 未删除，1 已删除
     */
    private Integer isDelete;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    private LocalDateTime updateTime;
    //每天可发送的消息
    private SendMsgVo sendMsgVo;

}
