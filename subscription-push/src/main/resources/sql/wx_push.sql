/*
 Navicat Premium Data Transfer

 Source Server         : 8.142.28.158guowei
 Source Server Type    : MySQL
 Source Server Version : 80030
 Source Host           : 8.142.28.158:3306
 Source Schema         : wx_push

 Target Server Type    : MySQL
 Target Server Version : 80030
 File Encoding         : 65001

 Date: 24/08/2022 11:21:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_send_msg
-- ----------------------------
DROP TABLE IF EXISTS `t_send_msg`;
CREATE TABLE `t_send_msg` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `user_id` int unsigned NOT NULL COMMENT 'user_id',
  `send_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '发送消息的名字',
  `send_msg` varchar(255) DEFAULT '今天又是元气满满的一天喔~',
  `is_delete` int DEFAULT '0' COMMENT '是否删除，0 未删除，1 已删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of t_send_msg
-- ----------------------------
BEGIN;
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (1, 1, '郭威', '', 0, '2022-08-23 13:09:18', '2022-08-23 14:09:18');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (2, 2, '邵慧', '宝宝，我爱你哦', 0, '2022-08-23 13:09:18', '2022-08-23 13:15:18');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (3, 1, '郭威', 'guoweimsg', 0, '2022-08-23 13:09:18', '2022-08-23 13:09:18');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (4, 1, '郭威', '发送消息', 0, '2022-08-23 14:09:18', '2022-08-23 14:09:18');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (5, 1, '郭威', '发送消息1', 0, '2022-08-23 14:09:18', '2022-08-23 14:12:18');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (6, 1, '郭威', '发送消息2', 0, '2022-08-23 13:53:49', '2022-08-23 13:53:49');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (7, 8, NULL, '今天又是元气满满的一天喔~', 0, NULL, NULL);
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (8, 9, '深巷', '你没有留言哦~~', 0, NULL, NULL);
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (9, 1, '郭威', '留言100', 0, '2022-08-23 15:17:36', '2022-08-23 15:17:36');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (10, 1, '郭威', '留言101', 0, '2022-08-23 15:18:56', '2022-08-23 15:18:56');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (11, 1, '郭威', '留言11', 0, '2022-08-23 15:41:14', '2022-08-23 15:41:14');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (12, 1, '郭威', '留言11', 0, '2022-08-23 15:57:51', '2022-08-23 15:57:51');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (13, 1, '郭威', '留言11', 0, '2022-08-23 16:15:45', '2022-08-23 16:15:45');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (14, 1, '郭威', '留言爱你啊', 0, '2022-08-23 16:16:49', '2022-08-23 16:16:49');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (15, 1, '郭威', '111', 0, '2022-08-23 16:21:48', '2022-08-23 16:21:48');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (16, 1, '郭威', '112', 0, '2022-08-23 16:32:39', '2022-08-23 16:32:39');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (17, 1, '郭威', '002', 0, '2022-08-23 16:38:50', '2022-08-23 16:38:50');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (18, 1, '郭威', '003', 0, '2022-08-23 16:49:40', '2022-08-23 16:49:40');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (19, 1, '郭威', '003', 0, '2022-08-23 16:49:48', '2022-08-23 16:49:48');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (20, 9, '深巷', '003', 0, '2022-08-23 16:55:44', '2022-08-23 16:55:44');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (21, 9, '深巷', '我爱你呀', 0, '2022-08-23 16:56:20', '2022-08-23 16:56:20');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (22, 9, '深巷', '我爱你呀', 0, '2022-08-23 16:58:34', '2022-08-23 16:58:34');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (23, 9, '深巷', '我爱你呀', 0, '2022-08-23 16:58:47', '2022-08-23 16:58:47');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (24, 9, '深巷', '我爱你呀', 0, '2022-08-23 17:02:15', '2022-08-23 17:02:15');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (25, 9, '深巷', '我爱你呀', 0, '2022-08-23 17:02:39', '2022-08-23 17:02:39');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (26, 9, '深巷', '是你能', 0, '2022-08-23 17:15:31', '2022-08-23 17:15:31');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (27, 9, '深巷', '是你能', 0, '2022-08-23 17:16:01', '2022-08-23 17:16:01');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (28, 9, '深巷', 'kkk', 0, '2022-08-23 17:22:46', '2022-08-23 17:22:46');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (29, 2, '邵慧', '我爱你呀', 0, '2022-08-23 17:23:02', '2022-08-23 17:23:02');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (30, 1, '郭威', '我爱你呀', 0, '2022-08-23 17:24:05', '2022-08-23 17:24:05');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (31, 2, '邵慧', '我爱你呀', 0, '2022-08-23 17:25:28', '2022-08-23 17:25:28');
INSERT INTO `t_send_msg` (`id`, `user_id`, `send_name`, `send_msg`, `is_delete`, `create_time`, `update_time`) VALUES (32, 1, '郭威', '模版2', 0, '2022-08-24 09:23:16', '2022-08-24 09:23:16');
COMMIT;

-- ----------------------------
-- Table structure for t_user_info
-- ----------------------------
DROP TABLE IF EXISTS `t_user_info`;
CREATE TABLE `t_user_info` (
  `user_id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `user_name` varchar(255) DEFAULT NULL,
  `send_name` varchar(255) DEFAULT NULL COMMENT '真实姓名',
  `open_id` varchar(255) NOT NULL,
  `city_code` varchar(255) NOT NULL DEFAULT '110105' COMMENT '默认北京朝阳',
  `birthday_time` varchar(255) NOT NULL DEFAULT '1998-01-01',
  `msg` varchar(255) NOT NULL DEFAULT '今天又是元气满满的一天喔~',
  `memorial_day_msg` varchar(255) DEFAULT NULL,
  `unique_msg` varchar(255) DEFAULT NULL,
  `is_delete` int DEFAULT '0' COMMENT '是否删除，0 未删除，1 已删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of t_user_info
-- ----------------------------
BEGIN;
INSERT INTO `t_user_info` (`user_id`, `user_name`, `send_name`, `open_id`, `city_code`, `birthday_time`, `msg`, `memorial_day_msg`, `unique_msg`, `is_delete`, `create_time`, `update_time`) VALUES (1, '郭威', '郭威', 'ofbVB55jygAEF_7vR9BO0eARMFHc', '110105', '1998-03-19', '今天又是元气满满的一天喔~', '', '', 0, '2022-08-23 13:09:18', '2022-08-23 13:09:18');
INSERT INTO `t_user_info` (`user_id`, `user_name`, `send_name`, `open_id`, `city_code`, `birthday_time`, `msg`, `memorial_day_msg`, `unique_msg`, `is_delete`, `create_time`, `update_time`) VALUES (2, '亲爱的慧慧小宝贝', '邵慧', 'ofbVB5_czGmCeonlSwSuTP-_LPy8', '130984', '1998-09-05', '今天依旧是超级爱你的一天喔~', '2017-11-25', '我爱你', 0, '2022-08-23 13:09:18', '2022-08-23 13:09:18');
INSERT INTO `t_user_info` (`user_id`, `user_name`, `send_name`, `open_id`, `city_code`, `birthday_time`, `msg`, `memorial_day_msg`, `unique_msg`, `is_delete`, `create_time`, `update_time`) VALUES (3, '郭威', NULL, 'ofbVB52WAGJE_71Bq616NBM_AG381', '110105', '1998-03-19', '今天又是元气满满的一天喔~', NULL, '唯一unique_msg', 1, '2022-08-23 14:14:50', '2022-08-23 14:14:50');
INSERT INTO `t_user_info` (`user_id`, `user_name`, `send_name`, `open_id`, `city_code`, `birthday_time`, `msg`, `memorial_day_msg`, `unique_msg`, `is_delete`, `create_time`, `update_time`) VALUES (4, '深巷', '深巷', 'ofbVB52WAGJE_71Bq616NBM_AG382', '110105', '1998-03-19', '今天又是元气满满的一天喔~', NULL, '唯一unique_msg', 1, '2022-08-23 14:25:41', '2022-08-23 14:25:41');
INSERT INTO `t_user_info` (`user_id`, `user_name`, `send_name`, `open_id`, `city_code`, `birthday_time`, `msg`, `memorial_day_msg`, `unique_msg`, `is_delete`, `create_time`, `update_time`) VALUES (5, '深巷', '深巷', 'ofbVB52WAGJE_71Bq616NBM_AG383', '110105', '1998-03-19', '今天又是元气满满的一天喔~', NULL, '唯一unique_msg', 1, '2022-08-23 14:26:18', '2022-08-23 14:26:18');
INSERT INTO `t_user_info` (`user_id`, `user_name`, `send_name`, `open_id`, `city_code`, `birthday_time`, `msg`, `memorial_day_msg`, `unique_msg`, `is_delete`, `create_time`, `update_time`) VALUES (6, '深巷', '深巷', 'ofbVB52WAGJE_71Bq616NBM_AG38', '110105', '1998-03-19', '今天又是元气满满的一天喔~', NULL, '唯一unique_msg', 1, '2022-08-23 14:27:01', '2022-08-23 14:27:01');
INSERT INTO `t_user_info` (`user_id`, `user_name`, `send_name`, `open_id`, `city_code`, `birthday_time`, `msg`, `memorial_day_msg`, `unique_msg`, `is_delete`, `create_time`, `update_time`) VALUES (7, '深巷', '深巷', 'ofbVB52WAGJE_71Bq616NBM_AG38', '110105', '1998-03-19', '今天又是元气满满的一天喔~', NULL, '唯一unique_msg', 1, '2022-08-23 14:35:09', '2022-08-23 14:35:09');
INSERT INTO `t_user_info` (`user_id`, `user_name`, `send_name`, `open_id`, `city_code`, `birthday_time`, `msg`, `memorial_day_msg`, `unique_msg`, `is_delete`, `create_time`, `update_time`) VALUES (8, '深巷', '深巷', 'ofbVB52WAGJE_71Bq616NBM_AG38', '110105', '1998-03-19', '今天又是元气满满的一天喔~', NULL, '唯一unique_msg', 1, '2022-08-23 14:40:12', '2022-08-23 14:40:12');
INSERT INTO `t_user_info` (`user_id`, `user_name`, `send_name`, `open_id`, `city_code`, `birthday_time`, `msg`, `memorial_day_msg`, `unique_msg`, `is_delete`, `create_time`, `update_time`) VALUES (9, '深巷', '深巷', 'ofbVB52WAGJE_71Bq616NBM_AG38', '110105', '1998-03-19', '今天又是元气满满的一天喔~', NULL, NULL, 0, '2022-08-23 14:45:51', '2022-08-23 14:45:51');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
